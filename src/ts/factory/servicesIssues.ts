///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppFactory {

    export interface IIssueFactory {
        getData():string[];
        getDataIndex(index:number):string;
        putData: (index:number, value:string) => void;
    }
    IssueFactory.$inject = ['$http'];

    function IssueFactory ($http) : IIssueFactory {

        var data: string[];
        var ubicacion1: ModelsDefinitions.Location;
        data = [
            'value 1',
            'value 2'
        ];
        var listIssues: Array<ModelsDefinitions.Issue>;
        var ubicacion2: ModelsDefinitions.Location;
        var ubicacion3: ModelsDefinitions.Location;
        var cliente1: ModelsDefinitions.Client;
        var cliente2: ModelsDefinitions.Client;
        var cliente3: ModelsDefinitions.Client;
        var user: ModelsDefinitions.User;
        var issue :ModelsDefinitions.Issue;
        var issue2 :ModelsDefinitions.Issue;
        var issue3 :ModelsDefinitions.Issue;

        ubicacion1 = {
            latitude: '4.806861',
            longitude: '-75.692032'
        };
        ubicacion2 = {
            latitude: '4.804530',
            longitude: '-75.686968'
        };
        ubicacion3 = {
            latitude: '4.804808',
            longitude: '-75.694693'
        };
        cliente1 = {
            name: 'Pedro perez',
            address: 'Cra 7 # 21-20',
            location: ubicacion1
        };
        cliente2 = {
            name: 'Fernando Diaz',
            address: 'Cra 7 # 21-20',
            location: ubicacion2
        };
        cliente3 = {
            name: 'Margarita Carmona',
            address: 'Cra 7 # 21-20',
            location: ubicacion3
        };

        user = {
            userName: 'Luis',
            password: '123',
            location: ubicacion1
        };
        issue = {
            id: 1,
            status: ModelsDefinitions.StatusOptions.Open,
            client: cliente1,
            assignee: user,
            assignationDate: new Date('01/04/2015'),
            creationDate: new Date(),
            dateLastModification: new Date(),
            info: 'Reparar torre Issue 1',
            comments: 'Cliente VIP',
            icon: ''
        };
        issue2 = {
            id: 2,
            status: ModelsDefinitions.StatusOptions.Close,
            client: cliente2,
            assignee: user,
            assignationDate: new Date('01/05/2010'),
            creationDate: new Date('01/05/2010'),
            dateLastModification: new Date('01/05/2010'),
            info: 'Reparar torre Issue 2',
            comments: 'Cliente VIP',
            icon: ''
        };
        issue3 = {
            id: 3,
            status: ModelsDefinitions.StatusOptions.Pending,
            client: cliente3,
            assignee: user,
            assignationDate: new Date('12/08/2011'),
            creationDate: new Date('12/08/2011'),
            dateLastModification: new Date('12/08/2011'),
            info: 'Reparar torre Issue 3',
            comments: 'Cliente VIP',
            icon: ''
        };


        return {
            getData: () => { return data; },
            getDataIndex: function (index: number) {
                return data[index];
            },
            putData: function (index:number, value:string) {
                data[index] = value;
            },
            getIssues: function () {
                listIssues = [issue, issue2, issue3];
                return listIssues;
            },
            getCliente: function () {
                return cliente1;
            },
            contarIssuesPendiente: function () {
                var i: number;
                var cantPendientes : number;
                cantPendientes = 0;
                for (i = 0; i < listIssues.length; i++) {
                    if (listIssues[i].status === ModelsDefinitions.StatusOptions.Pending) {
                        cantPendientes++;
                    }
                }
                return cantPendientes;
            }
        };
    }
    angular.module('ASTApp.factory')
        .factory('ASTApp.factory.IssueFactory', IssueFactory);
}
