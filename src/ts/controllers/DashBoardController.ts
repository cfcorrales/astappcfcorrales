///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppControllers {
    'use strict';

    class DashBoardController {

        data: string[];
        map;
        marker;
        issues: Array<ModelsDefinitions.Issue>;


        static $inject = ['getListIssues'];
        constructor(getListIssues) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.map = { center: { latitude:  4.807326, longitude:  -75.692637 }, zoom: 14 };
            this.issues = getListIssues;
            this.calcularAlerta();
        }

        calcularAlerta() : void {
            var i : number;
            var resta : number;
            for (i = 0; i < this.issues.length; i++) {
                resta = Math.abs(this.restarFechas(this.issues[i].assignationDate.getTime(), new Date().getTime()));
                if (resta > 24 && this.issues[i].status === ModelsDefinitions.StatusOptions.Open) {
                    this.issues[i].icon = 'img/WarningRojo.png';
                }
                if (resta <= 24 && this.issues[i].status === ModelsDefinitions.StatusOptions.Open) {
                    this.issues[i].icon = 'img/WarningAmarillo.png';
                }
                if (resta > 24 && this.issues[i].status === ModelsDefinitions.StatusOptions.Pending) {
                    this.issues[i].icon = 'img/warningpurple.png';
                }
                if (resta <= 24 && this.issues[i].status === ModelsDefinitions.StatusOptions.Pending) {
                    this.issues[i].icon = 'img/warningNaranja.png';
                }
            }
        }

        restarFechas(fecha1: number, fecha2: number) : number {
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        }
    }

    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.DashBoardController', DashBoardController);
}

//For use inside routes definition app.ts:
//.state('dashBoardController', {
//                        url: '/dashBoardController',
//                        templateUrl: 'templates/dashBoardController-template.html',
//                        controller: 'DashBoardControllers.DashBoardControllerController as dashBoardController'
//                    });

//For use inside template:
//    {{dashBoardController.data}}

//Check dependencies inside app.ts
//    angular.module('DashBoardControllers.controllers', []);
//    angular.module('app', ['ionic', 'DashBoardControllers.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/dashBoardControllercontroller.js"></script>