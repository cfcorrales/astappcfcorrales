///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities/entities.ts"/>

module ASTAppControllers {
    'use strict';

    interface IListIssuesController {
         label: string;
         value: string;
    }

    class ListIssuesController {

        data: string[];
        listIssues: Array<ModelsDefinitions.Issue>;
        filtro;
        options : Array<IListIssuesController>;
        estados;
        static $inject = ['getListIssues'];
        constructor(getListIssues) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.listIssues = getListIssues;
            this.filtro = '-assignationDate';
            this.options = [
                { label: 'Fecha', value: 'assignationDate' },
                { label: 'Estado', value: 'status' }
            ];
            this.estados = ModelsDefinitions.StatusOptions;

        }
    }

    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.ListIssuesController', ListIssuesController);
}

//For use inside routes definition app.ts:
//.state('listController', {
//                        url: '/listController',
//                        templateUrl: 'templates/listController-template.html',
//                        controller: 'ASTApp.ListControllerController as listController'
//                    });

//For use inside template:
//    {{listController.data}}

//Check dependencies inside app.ts
//    angular.module('ASTApp.controllers', []);
//    angular.module('app', ['ionic', 'ASTApp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/listControllercontroller.js"></script>