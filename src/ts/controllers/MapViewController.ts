///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities/entities.ts"/>

module ASTAppControllers {
    'use strict';

    class MapaController {
        marker;
        data: string[];
        map;
        options;
        cliente: ModelsDefinitions.Client;
        static $inject = ['getClientesLocal'];
        constructor(getClientesLocal) {
            //Controller Body
            this.cliente = getClientesLocal;
            this.data = ['valor 1', 'valor 2'];
            this.map = { center: { latitude:  4.807326, longitude:  -75.692637 }, zoom: 14 };
            this.options = {draggable: true};
            this.marker = {
                coords: this.cliente.location,
                id: 0,
                options: {draggable: true},
                urlIcono: 'imagenes/car.png'
            };

        }


    }


    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.MapaController', MapaController);
}

//For use inside routes definition app.ts:
//.state('mapa', {
//                        url: '/mapa',
//                        templateUrl: 'templates/mapa-template.html',
//                        controller: 'ASTApp.MapaController as mapa'
//                    });

//For use inside template:
//    {{mapa.data}}

//Check dependencies inside app.ts
//    angular.module('ASTApp.controllers', []);
//    angular.module('app', ['ionic', 'ASTApp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/mapacontroller.js"></script>