///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppControllers {
    'use strict';
    class DashBoardRootController {
        data: string[];
        listIssues: Array<ModelsDefinitions.Issue>;
        static $inject = ['getListIssues'];
        constructor(getListIssues) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.listIssues = getListIssues;
        }
    }

    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.DashBoardRootController', DashBoardRootController);
}

//For use inside routes definition app.ts:
//.state('dashBoardRoot', {
//                        url: '/dashBoardRoot',
//                        templateUrl: 'templates/dashBoardRoot-template.html',
//                        controller: 'ASTApp.DashBoardRootController as dashBoardRoot'
//                    });

//For use inside template:
//    {{dashBoardRoot.data}}

//Check dependencies inside app.ts
//    angular.module('ASTApp.controllers', []);
//    angular.module('app', ['ionic', 'ASTApp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/dashBoardRootcontroller.js"></script>
