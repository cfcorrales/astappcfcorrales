///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities/entities.ts"/>

module ASTAppControllers {
    'use strict';

    class IssueDetailController {

        data: string[];
        issue: ModelsDefinitions.Issue;
        map;
        marker;
        static $inject = ['getCliente'];
        constructor(getCliente) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.issue = getCliente;
            this.map = { center: { latitude:  4.807326, longitude:  -75.692637 }, zoom: 14 };

            this.marker = {
                coords: this.issue.client.location,
                id: 0,
                options: {draggable: true},
                urlIcono: 'imagenes/car.png'
            };

        }
    }

    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.IssueDetailController', IssueDetailController);
}

//For use inside routes definition app.ts:
//.state('issueDetailController', {
//                        url: '/issueDetailController',
//                        templateUrl: 'templates/issueDetailController-template.html',
//                        controller: 'IssueDetail.IssueDetailControllerController as issueDetailController'
//                    });

//For use inside template:
//    {{issueDetailController.data}}

//Check dependencies inside app.ts
//    angular.module('IssueDetail.controllers', []);
//    angular.module('app', ['ionic', 'IssueDetail.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/issueDetailControllercontroller.js"></script>