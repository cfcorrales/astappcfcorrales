///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module ASTAppControllers {
    'use strict';
    class DashBoardListController {
        data : string[];
        issues : Array<ModelsDefinitions.Issue>;
        cantidadPendientes: number;
        estado : ModelsDefinitions.StatusOptions;
        static $inject = ['getListIssues', 'getCantPendientes'];
        constructor(getListIssues, getcantPendientes) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.issues = getListIssues;
            this.cantidadPendientes = getcantPendientes;
            this.estado = ModelsDefinitions.StatusOptions.Close;
        }
    }
    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.DashBoardListController', DashBoardListController);
}

//For use inside routes definition app.ts:
//.state('dashBoardList', {
//                        url: '/dashBoardList',
//                        templateUrl: 'templates/dashBoardList-template.html',
//                        controller: 'AstApp.DashBoardListController as dashBoardList'
//                    });

//For use inside template:
//    {{dashBoardList.data}}

//Check dependencies inside app.ts
//    angular.module('AstApp.controllers', []);
//    angular.module('app', ['ionic', 'AstApp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/dashBoardListcontroller.js"></script>
