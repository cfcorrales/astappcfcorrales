///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../entities/entities.ts"/>

module ASTAppControllers {
    'use strict';

    class ClienteController {

        data: string[];
        cliente: ModelsDefinitions.Client;
        static $inject = ['getClientesLocal'];
        constructor(getClientesLocal) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.cliente = getClientesLocal;

        }
    }

    angular.module('ASTApp.controllers')
        .controller('ASTApp.controllers.ClienteController', ClienteController);
}

//For use inside routes definition app.ts:
//.state('cliente', {
//                        url: '/cliente',
//                        templateUrl: 'templates/cliente-template.html',
//                        controller: 'Cliente.ClienteController as cliente'
//                    });

//For use inside template:
//    {{cliente.data}}

//Check dependencies inside app.ts
//    angular.module('Cliente.controllers', []);
//    angular.module('app', ['ionic', 'Cliente.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/clientecontroller.js"></script>