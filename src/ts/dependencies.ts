///<reference path="../../tools/typings/tsd.d.ts"/>
///<reference path="../../tools/typings/typescriptApp.d.ts"/>
(function(){
    angular.module('dependencies', ['ASTApp.controllers', 'ASTApp.factory']);
    angular.module('ASTApp.controllers', []);
    angular.module('ASTApp.factory', []);
})();
