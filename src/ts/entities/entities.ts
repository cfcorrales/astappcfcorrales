///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
module ModelsDefinitions {
    export enum StatusOptions {
        Open = 0,
        Close = 1,
        Pending = 2
    }

   export interface Location {
        latitude: string;
        longitude: string
    }

    export interface User {
        userName: string;
        password: string;
        location: Location
    }

    export interface Issue {
        id: number;
        status: StatusOptions;
        client: Client;
        assignee: User;
        assignationDate: Date;
        creationDate: Date;
        dateLastModification: Date;
        info: string;
        comments: string;
        icon: string
    }

    export interface Client {
        name: string;
        address: string;
        location: Location
    }
}