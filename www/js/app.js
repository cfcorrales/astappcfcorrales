///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />
///<reference path="entities/entities.ts"/>
(function () {
    angular.module('app', ['ionic', 'dependencies', 'ngCordova', 'uiGmapgoogle-maps']).run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
            }
        });
    }).filter('Cerradas', function () {
        return function (input, estado) {
            var size = input.length;
            var out = [];
            var i;
            for (i = 0; i < size; i++) {
                if (input[i].status !== estado) {
                    out.push(input[i]);
                }
            }
            return out;
        };
    }).config(['$stateProvider', '$urlRouterProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) {
        var listIssues;
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyATPx0Z-w2U9Ac1WDgIGW6awopLOh2JoII',
            v: '3.17',
            libraries: 'weather,geometry,visualization'
        });
        $urlRouterProvider.otherwise('/login');
        $stateProvider.state('login', {
            url: '/login',
            views: {
                login: {
                    templateUrl: 'templates/Login-template.html',
                    controller: 'ASTApp.controllers.LoginViewController as loginController'
                }
            }
        }).state('cliente', {
            url: '/cliente',
            views: {
                cliente: {
                    templateUrl: 'templates/Client-template.html',
                    controller: 'ASTApp.controllers.ClienteController as clienteController'
                }
            },
            resolve: {
                getClientesLocal: ['ASTApp.factory.IssueFactory', function (IssueFactory) {
                    return IssueFactory.getCliente();
                }]
            }
        }).state('Issues', {
            url: '/Issues',
            views: {
                Issues: {
                    templateUrl: 'templates/ListIssues-template.html',
                    controller: 'ASTApp.controllers.ListIssuesController as listIssuesController'
                }
            },
            resolve: {
                getListIssues: ['ASTApp.factory.IssueFactory', function (IssueFactory) {
                    listIssues = IssueFactory.getIssues();
                    return listIssues;
                }]
            }
        }).state('Issue-detail', {
            url: '/Issue/:id',
            views: {
                'Issues': {
                    templateUrl: 'templates/IssueDetail-template.html',
                    controller: 'ASTApp.controllers.IssueDetailController as issueDetailController'
                }
            },
            resolve: {
                getCliente: ['$stateParams', function ($stateParams) {
                    var i;
                    for (i = 0; i < listIssues.length; i++) {
                        if (listIssues[i].id === parseInt($stateParams.id, 10)) {
                            return listIssues[i];
                        }
                    }
                }]
            }
        }).state('mapaView', {
            url: '/mapaView',
            views: {
                mapaView: {
                    templateUrl: 'templates/MapView-template.html',
                    controller: 'ASTApp.controllers.MapaController as mapaController'
                }
            },
            resolve: {
                getClientesLocal: ['ASTApp.factory.IssueFactory', function (IssueFactory) {
                    return IssueFactory.getCliente();
                }]
            }
        }).state('dashBoard', {
            url: '/dashBoard',
            views: {
                dashBoard: {
                    templateUrl: 'templates/DashBoard-template.html',
                    controller: 'ASTApp.controllers.DashBoardController as dashBoardController'
                }
            },
            resolve: {
                getListIssues: ['ASTApp.factory.IssueFactory', function (IssueFactory) {
                    return IssueFactory.getIssues();
                }]
            }
        }).state('dashBoardList', {
            url: '/dashBoardList',
            views: {
                dashBoardList: {
                    templateUrl: 'templates/DashBoardList-template.html',
                    controller: 'ASTApp.controllers.DashBoardListController as dashBoardControllerList'
                }
            },
            resolve: {
                getListIssues: ['ASTApp.factory.IssueFactory', function (IssueFactory) {
                    return IssueFactory.getIssues();
                }],
                getCantPendientes: ['ASTApp.factory.IssueFactory', function (IssueFactory) {
                    return IssueFactory.contarIssuesPendiente();
                }]
            }
        });
    }]);
})();

//# sourceMappingURL=app.js.map