///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTAppControllers;
(function (ASTAppControllers) {
    'use strict';
    var LoginViewController = (function () {
        function LoginViewController($window) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
        }
        LoginViewController.$inject = ['$window'];
        return LoginViewController;
    })();
    angular.module('ASTApp.controllers').controller('ASTApp.controllers.LoginViewController', LoginViewController);
})(ASTAppControllers || (ASTAppControllers = {}));
//For use inside routes definition app.ts:
//.state('loginViewController', {
//                        url: '/loginViewController',
//                        templateUrl: 'templates/loginViewController-template.html',
//                        controller: 'ASTApp.LoginViewControllerController as loginViewController'
//                    });
//For use inside template:
//    {{loginViewController.data}}
//Check dependencies inside app.ts
//    angular.module('ASTApp.controllers', []);
//    angular.module('app', ['ionic', 'ASTApp.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/loginViewControllercontroller.js"></script> 

//# sourceMappingURL=../controllers/LoginViewController.js.map