///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ASTAppControllers;
(function (ASTAppControllers) {
    'use strict';
    var DashBoardController = (function () {
        function DashBoardController(getListIssues) {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.map = { center: { latitude: 4.807326, longitude: -75.692637 }, zoom: 14 };
            this.issues = getListIssues;
            this.calcularAlerta();
        }
        DashBoardController.prototype.calcularAlerta = function () {
            var i;
            var resta;
            for (i = 0; i < this.issues.length; i++) {
                resta = Math.abs(this.restarFechas(this.issues[i].assignationDate.getTime(), new Date().getTime()));
                if (resta > 24 && this.issues[i].status === 0 /* Open */) {
                    this.issues[i].icon = 'img/WarningRojo.png';
                }
                if (resta <= 24 && this.issues[i].status === 0 /* Open */) {
                    this.issues[i].icon = 'img/WarningAmarillo.png';
                }
                if (resta > 24 && this.issues[i].status === 2 /* Pending */) {
                    this.issues[i].icon = 'img/warningpurple.png';
                }
                if (resta <= 24 && this.issues[i].status === 2 /* Pending */) {
                    this.issues[i].icon = 'img/warningNaranja.png';
                }
            }
        };
        DashBoardController.prototype.restarFechas = function (fecha1, fecha2) {
            return (fecha2 - fecha1) / 1000 / 60 / 60;
        };
        DashBoardController.$inject = ['getListIssues'];
        return DashBoardController;
    })();
    angular.module('ASTApp.controllers').controller('ASTApp.controllers.DashBoardController', DashBoardController);
})(ASTAppControllers || (ASTAppControllers = {}));
//For use inside routes definition app.ts:
//.state('dashBoardController', {
//                        url: '/dashBoardController',
//                        templateUrl: 'templates/dashBoardController-template.html',
//                        controller: 'DashBoardControllers.DashBoardControllerController as dashBoardController'
//                    });
//For use inside template:
//    {{dashBoardController.data}}
//Check dependencies inside app.ts
//    angular.module('DashBoardControllers.controllers', []);
//    angular.module('app', ['ionic', 'DashBoardControllers.controllers'])
//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/dashBoardControllercontroller.js"></script> 

//# sourceMappingURL=../controllers/DashBoardController.js.map