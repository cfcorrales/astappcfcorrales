///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />
var ModelsDefinitions;
(function (ModelsDefinitions) {
    (function (StatusOptions) {
        StatusOptions[StatusOptions["Open"] = 0] = "Open";
        StatusOptions[StatusOptions["Close"] = 1] = "Close";
        StatusOptions[StatusOptions["Pending"] = 2] = "Pending";
    })(ModelsDefinitions.StatusOptions || (ModelsDefinitions.StatusOptions = {}));
    var StatusOptions = ModelsDefinitions.StatusOptions;
})(ModelsDefinitions || (ModelsDefinitions = {}));

//# sourceMappingURL=../entities/entities.js.map