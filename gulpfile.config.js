'use strict';
var GulpConfig = (function () {
    function GulpConfig() {
        //Got tired of scrolling through all the comments so removed them
        //Don't hurt me AC :-)
        this.source = './src/';
        this.sourceApp = this.source + 'ts/';
        this.sourceTestApp = this.source + 'test/';

        this.destination = './www';
        this.tsOutputPath = this.destination + '/js';
        this.tsTestOutputPath = './src/test';
        this.allJavaScript = [this.destination + '/js/**/*.js'];
        this.allTypeScript = this.sourceApp + '/**/*.ts';
        this.allTestTypeScript = this.sourceTestApp + '**/*.ts';

        this.typings = './tools/typings/';
        this.libraryTypeScriptDefinitions = './tools/typings/**/*.ts';
        this.appTypeScriptReferences = this.typings + 'typescriptApp.d.ts';
    }
    return GulpConfig;
})();
module.exports = GulpConfig;
